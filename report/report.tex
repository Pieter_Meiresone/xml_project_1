\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{hyperref}

\lstset{basicstyle=\small, language=XML}

\title{XML and Web Technologies}
\subtitle{Project Assignment 1: XML Schema Definition}
\author{Zhanna Kononenko and Pieter Meiresone\\
\small{Mail: \{Zhanna.Kononenko, pmeireso\}@vub.ac.be}}
\date{April 2015}

\begin{document}

\maketitle

\section{Introduction}
The XML representation of the bookshop was created within this project. The shop is divided into two departments and has several different types of products. For validation purposes the XML Schema Definition was generated. The different types will be presented in the XSD Schema and, finally, the testing of Schema will be provided by means of a different examples of XML document reflecting various book shop configurations.

\section{Create an XML Schema}
To create the schema we could simply follow the structure in the XML document and define each element as we find it. We will start with the standard xs:schema element that defines a schema, where we use standard namespace xs:
\begin{lstlisting}
 <xs:schema targetNamespace="http://www.bookshop.org"
         elementFormDefault="qualified">
\end{lstlisting}
Next, we have to define the ``bookshop'' element. This element has an attribute and it contains other elements, therefore we consider it as a complex type.
\begin{lstlisting}
 <xs:element name="bookshop" type="b:bookshopType"/>
\end{lstlisting}
``bookshopTyp'' is a complex type that contains two elements: ``scientificDepartment'' and ``leisureDepartment''. The $<$all$>$ indicator was used, because ``scientificDepartment'' and ``leisureDepartment'' can appear in any order and only at most one time.
\begin{lstlisting}
 <xs:complexType name="bookshopType">
  <xs:all>
   <xs:element name="scientificDepartment" minOccurs="0" type="b:scientificType"/>
   <xs:element name="leisureDepartment" minOccurs="0" type="b:leisureType"/>
  </xs:all>
 </xs:complexType>
\end{lstlisting}
``scientificType" is a complex type that contains two types of the products in the scientific department: scientific books and scientific journals. The choice element allows only one of the elements contained in the $<$choice$>$ declaration to be present within the containing element. The $<$minOccurs$>$ is 0. And the upper bound - $<$maxOccurs$>$ - is unlimited. This way, the books in the scientific department can be placed in the $<$scientificDeparment$>$ element in any order.
\begin{lstlisting}
 <xs:complexType name="scientificType">
  <xs:choice minOccurs="0" maxOccurs="unbounded">
   <xs:element name="book" type="b:scientificBookType"/>
   <xs:element name="journal" type="b:journalType"/>
  </xs:choice>
 </xs:complexType>
\end{lstlisting}
``leisureType" is a complex type that contains two types of the products in the leisure department: leisure books and leisure periodicals. The structure for the leisure type is identical with that of the scientificType.
\begin{lstlisting}
 <xs:complexType name="leisureType">
  <xs:choice minOccurs="0" maxOccurs="unbounded">
   <xs:element name="book" type="b:leisureBookType"/>
   <xs:element name="periodical" type="b:periodicalType"/>
  </xs:choice>
 </xs:complexType>
\end{lstlisting}
There was chosen to implement a type  hierarchy for book elements, this means that ``scientificType" and ``leisureType" both inherit from the ``abstractBookType". The main advantage of this hierarchy is that there is less redundancy in the XML Schema (this is similar in object-oriented programming languages). However, introducing this type of hierarchy forbids to use the $<$all$>$ content model in a ``complexType". Thus, $<$sequence$>$ is used instead. This element specifies that all elements have to appear in the order as described in the XML Schema.
\begin{lstlisting}
 <xs:complexType name="abstractBookType" abstract="true">
  <xs:sequence>
   <xs:element name="title" type="xs:string"/>
   <xs:element name="publisher" type="xs:string"/>
   <xs:element name="edition" type="xs:string" minOccurs="0"/>
  </xs:sequence>
  <xs:attribute name="year" type="xs:nonNegativeInteger" use="required"/>
 </xs:complexType>
\end{lstlisting}
"scientificBookType" extends the previously mentioned "abstractBookType", and also adds a sequence of elements: publisher, abstract, edition, ISBN and the choice between list of authors and list of editors. The elements abstract, edition and ISBN are optional fields, as declared by their occurrence indicator minOccurs=0.
\begin{lstlisting}
 <xs:complexType name="scientificBookType">
  <xs:complexContent>
   <xs:extension base="b:abstractBookType">
    <xs:sequence>
     <xs:choice>
      <xs:element name="authors" type="b:authorListType"/>
      <xs:element name="editors" type="b:editorListType"/>
     </xs:choice>
     <xs:element name="abstract" type="xs:string" minOccurs="0"/>
     <xs:element name="ISBN" type="b:ISBNType" minOccurs="0"/>
    </xs:sequence>
   </xs:extension>
  </xs:complexContent>
 </xs:complexType>
\end{lstlisting}
The ISBN number is represented by a simple type in the XML Schema. An ISBN number has 10 or 13 digits, these digits are divided into blocks with hyphens. These blocks can have variable lengths\footnote{More information about the ISBN can be found at \url{http://en.wikipedia.org/wiki/International_Standard_Book_Number}}, Thus, the ISBN number can be stored at a string with following pattern values.
\begin{lstlisting}
 <xs:simpleType name="ISBNType">
  <xs:restriction base="xs:string">
   <xs:pattern value="([0-9](-?)){10}|([0-9](-?)){13}"/>
  </xs:restriction>
 </xs:simpleType>
\end{lstlisting}
``journalType" extends ``abstractBookType" as well. This type has the mandatory elements title, volume, number, authors and editors. It also has the optional elements publisher and impactFactor.
\begin{lstlisting}
 <xs:complexType name="journalType">
  <xs:sequence>
   <xs:element name="title" type="xs:string"/>
   <xs:element name="volume" type="xs:integer"/>
   <xs:element name="number" type="xs:integer"/>
  <xs:choice>
   <xs:element name="authors" type="b:authorListType"/>
   <xs:element name="editors" type="b:editorListType"/>
  </xs:choice>
  <xs:element name="publisher" type="xs:string" minOccurs="0"/>
  <xs:element name="impactFactor" minOccurs="0">
 <xs:complexType>
\end{lstlisting}
``leisureBookType" also extends the ``abstractBookType" and provides some mandatory and optional elements.
\begin{lstlisting}
 <xs:complexType name="leisureBookType">
  <xs:complexContent>
   <xs:extension base="b:abstractBookType">
    <xs:sequence>
     <xs:element name="author" type="xs:string" maxOccurs="unbounded"/>
     <xs:element name="numberOfPages" type="xs:nonNegativeInteger" minOccurs="0"/>
    </xs:sequence>
    <xs:attribute name="genre" type="b:genreType" use="required"/>
   </xs:extension>
  </xs:complexContent>
 </xs:complexType>
\end{lstlisting}
``periodicalType" contains the mandatory elements: title, price and publisher
\begin{lstlisting}
 <xs:complexType name="periodicalType">
  <xs:all>
   <xs:element name="title" type="xs:string"/>
   <xs:element name="price" type="b:priceType"/>
   <xs:element name="publisher" type="xs:string"/>
  </xs:all>
</xs:complexType>
\end{lstlisting}
The ``priceType''  is defined with a mandatory ``concurrency'' attribute. Furthermore the price should have exactly two digits after the decimal point. There requirements can be realized with an ``extension'' and ``restriction'' element. Since these two elements can't be merged in one block, we created an intermediate type ``exactlyTwoAfterDecimal''. This type applies the restriction, then we make an extension for the attribute ``concurrency'' in the ``priceType''.
\begin{lstlisting}
  <xs:simpleType name="exactlyTwoAfterDecimal">
    <xs:restriction base="xs:string">
        <xs:pattern value="-?\d+\.\d{2}"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:complexType name="priceType">
    <xs:simpleContent>
      <xs:extension base="b:exactlyTwoAfterDecimal">
        <xs:attribute name="currency" type="xs:string" use="required"/>
      </xs:extension>
    </xs:simpleContent>
  </xs:complexType>
\end{lstlisting}

\section{Some XML examples}

\begin{enumerate}
\item The ISBN has a specific format and inputting an inappropriate number of digits leads to an error.
\begin{lstlisting}
 <book year="2005">
     <title>An Introduction to XML and Web Technologies</title>
                    ...
     <!-- Valid ISBN, contains 13 digits. -->
     <ISBN>978-0-321-26966-9</ISBN>
 </book>
\end{lstlisting}
\item The format of the price should follow the schema, which is two decimals after the dot and a concurrency attribute, otherwise it will lead to an error. For example, following XML snippet is not valid, since there are no fractional digits.
\begin{lstlisting}
 <periodical>
                ...
  <price currency="USD">12</price>
               ...
 </periodical>
\end{lstlisting}
\item If an unexpected value is used as value for the genre attribute, the validation will fail. This is illustrated below.
\begin{lstlisting}
<!-- This is a valid -->
<book genre="thriller" ...> 
<!-- This is NOT a valid genre, validation will fail -->
<book genre="satire" ...> 
\end{lstlisting}
\item In scientific products a list of all authors or a list of editors should be present, but not both. A valid example:
\begin{lstlisting}
<book genre="thriller" year="1999">
                ...
  <authors>
    <author>Anders Moller</author>
    <author>Michael Schwartzbach</author>
  </authors>
                ...
</book>
\end{lstlisting}
An example that isn't valid:
\begin{lstlisting}
<journal year="2014">
  ...
  <editors>
    <editor>Mariette DiChristina</editor>
  </editors>
  <authors>
    <author>Pieter Meiresone</author>
    <author>Michael Schwartzbach</author>
  </authors>
  ...
</journal>
\end{lstlisting}
\end{enumerate}

\section{Conclusion}
While XML Schema is more expressive than DTD, we had to limit ourself a little bit and accept that some things are not possible in XML Schema. For example, the use of $<$sequence$>$ instead of $<$all$>$ obliged us to let the elements appear in order. This way the requirements are not exactly met.
\\
\\
In the archive file an example XML file can be found. This document can be validated with the `XSDValidation.jar' and `schema.xsd'.
\begin{center}
	java -jar XSDValidation.jar schema.xsd example.xml
\end{center}

\end{document}
